import { Inject, Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';

export enum BackgroundBodyClassEnum {
  DARK = 'background-dark',
  WHITE = 'background-white',
}

@Injectable()
export class StyleService {
  private renderer: Renderer2;

  constructor(@Inject(DOCUMENT) private document: Document, rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  public setBodyBackground(background: BackgroundBodyClassEnum) {
    switch (background) {
      case BackgroundBodyClassEnum.DARK:
        this.setDarkBodyBackground();
        break;

      case BackgroundBodyClassEnum.WHITE:
        this.setWhiteBodyBackground();
        break;

      default:
        this.setDarkBodyBackground();
    }
  }

  private setDarkBodyBackground() {
    this.renderer.addClass(document.body, BackgroundBodyClassEnum.DARK);
    this.renderer.removeClass(document.body, BackgroundBodyClassEnum.WHITE);
  }

  private setWhiteBodyBackground() {
    this.renderer.addClass(document.body, BackgroundBodyClassEnum.WHITE);
    this.renderer.removeClass(document.body, BackgroundBodyClassEnum.DARK);
  }
}

import { TranslateLoader } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';

export class TranslationsLoader implements TranslateLoader {
  // load translations, if you need inject service - use factory in app.module.ts and constructor in this class
  public getTranslation(lang: string): Observable<any> {
    if (lang === 'cs') {
      return of({
        TRANSLATION_KEY1: 'testovaci preklad 1',
        TRANSLATION_KEY2: 'testovaci preklad 2',
      });
    }

    if (lang === 'en') {
      return of({
        TRANSLATION_KEY1: 'testing translation',
        TRANSLATION_KEY2: 'testing translation',
      });
    }

    if (lang === '') {
      throw new Error('Any language defined!!');
    }
  }
}

import { MissingTranslationHandler, MissingTranslationHandlerParams } from '@ngx-translate/core';

/**
 * Handling missing translation in specific key - return empty string into target element
 */
export class MissingTranslationsHandler implements MissingTranslationHandler {
  public handle(params: MissingTranslationHandlerParams): string {
    // log missing translations or whatever you want
    return '';
  }
}

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { UsersService } from '../../api/users/users.service';
import { User } from '../../api/users/users.interface';

@Injectable()
export class AuthorizedGuardService implements CanActivate {
  constructor(private router: Router, private userService: UsersService) {}

  public canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.userService.users(false).pipe(
      map((users: User[]) => {
        const currentUser: User = this.userService.loggedUser.value;

        if (!currentUser || !users) {
          this.logoutAndRedirect();
          return false;
        }
        const findUser: User = this.userService.findUserByUserName(currentUser.user.displayName, users);

        if (!findUser) {
          this.logoutAndRedirect();
          return false;
        }
        return true;
      })
    );
  }

  private logoutAndRedirect(): void {
    this.userService.clearUserData();
    this.router.navigate(['username']);
  }
}

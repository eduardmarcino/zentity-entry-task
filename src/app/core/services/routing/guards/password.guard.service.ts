import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { UsersService } from '../../api/users/users.service';
import { User } from '../../api/users/users.interface';

@Injectable()
export class PasswordGuardService implements CanActivate {
  constructor(private router: Router, private userService: UsersService) {}

  public canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.userService.userByUserName.pipe(
      map((user: User) => {
        if (!user) {
          this.router.navigate(['username']);
          return false;
        }
        return true;
      })
    );
  }
}

import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

@Injectable()
export class SeoService {
  private defaultTitle = 'Zentity - Entry task';

  constructor(private titleService: Title) {}

  public configureSeoOnNavigation(route: ActivatedRoute) {
    let { title } = route.snapshot.data;
    title = (title || '') + ' | ' + this.defaultTitle;
    this.titleService.setTitle(title);
  }
}

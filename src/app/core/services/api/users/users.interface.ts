export interface User {
  onboarded: boolean;
  user: UserDetail;
}

export interface UserDetail {
  name: string;
  surname: string;
  displayName: string;
  password: string;
  contact: Contact;
}

export interface GpsLocation {
  latitude: number;
  longitude: number;
}

export interface Address {
  streetName: string;
  streetNumber: string;
  suburb: string;
  stateOrProvince: string;
  complex: string;
  postalCode: string;
  city: string;
  country: string;
  addressString: string;
}

export interface Location {
  id: string;
  name: string;
  location: GpsLocation;
  address: Address;
}

export interface SocialNetwork {
  id: string;
  name: string;
  url: string;
}

export interface Contact {
  email: string;
  phoneNumber: string;
  locations: Location[];
  socialNetworks: SocialNetwork[];
}

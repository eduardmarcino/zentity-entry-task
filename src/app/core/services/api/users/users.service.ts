import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UserDetail, User } from './users.interface';
import { map } from 'rxjs/operators';
import { Cacheable } from 'ts-cacheable';

@Injectable()
export class UsersService {
  public userByUserName = new BehaviorSubject<User>(null);
  public loggedUser = new BehaviorSubject<User>(null);

  constructor(private http: HttpClient) {}

  public users(cache: boolean = true): Observable<User[]> {
    return cache ? this.getUsersFromCache() : this.getUsers();
  }

  public clearUserData(): void {
    this.userByUserName.next(null);
    this.loggedUser.next(null);
  }

  public findUserByUserName(username: string, users: User[]): User | null {
    return users.find((user: User) => user.user.displayName.toLowerCase() === username.toLowerCase());
  }

  private getUsers(): Observable<User[]> {
    return this.http.get<User>('data.json').pipe(map((data: User) => [data]));
  }

  @Cacheable()
  private getUsersFromCache(): Observable<User[]> {
    return this.getUsers();
  }
}

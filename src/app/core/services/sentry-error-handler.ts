import { ErrorHandler, Inject, Injectable } from '@angular/core';
import * as Sentry from '@sentry/browser';
import * as moment from 'moment-mini-ts';

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor(@Inject('windowObject') private window: any) {}

  public handleError(error) {
    if (error.message && error.message.includes('Error: Loading chunk')) {
      // invalid chunk
      const reloadedAt = this.window.sessionStorage.getItem('reloadedAt');
      if (!reloadedAt || moment(reloadedAt).add('3', 'minutes').toDate() < new Date()) {
        this.window.sessionStorage.setItem('reloadedAt', moment().toDate().toString());
        this.window.location.reload(true);
      }
    } else {
      console.error(error);
      // log errors into 3rd party
      //Sentry.captureException(error.originalError || error);
    }
  }
}

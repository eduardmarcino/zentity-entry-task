import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AbstractControl, Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../services/api/users/users.service';
import { take } from 'rxjs/operators';
import { UserDetail, User } from '../services/api/users/users.interface';
import { MustMatch } from '../services/utils/form-match.validator';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'profile',
  styleUrls: ['./profile.component.scss'],
  templateUrl: 'profile.component.html',
})
export class ProfileComponent implements OnInit, OnDestroy {
  public loggedUser: User;
  public userEditForm: FormGroup;
  private ngUnsubscribe: Subject<boolean> = new Subject<boolean>();

  constructor(private formBuilder: FormBuilder, private usersService: UsersService, private router: Router) {}

  public ngOnInit(): void {
    this.usersService.loggedUser.pipe(take(1)).subscribe((user: User) => {
      this.loggedUser = user;

      this.userEditForm = this.formBuilder.group({
        name: [`${this.loggedUser.user.name} ${this.loggedUser.user.surname}`, [Validators.required]],
        username: [this.loggedUser.user.displayName],
        address: [
          `${this.loggedUser.user.contact.locations[0].address.streetName} ${this.loggedUser.user.contact.locations[0].address.streetNumber}`,
        ],
        city: [this.loggedUser.user.contact.locations[0].address.city],
        postalCode: [this.loggedUser.user.contact.locations[0].address.postalCode],
        email: [this.loggedUser.user.contact.email],
        phone: [this.loggedUser.user.contact.phoneNumber],
        socialMedia: [this.loggedUser.user.contact.socialNetworks[0].name],
      });
    });
  }

  public delete(control: AbstractControl): void {
    control.setValue('');
  }

  public edit(): void {
    if (!this.userEditForm.valid) {
      return;
    }
    console.log(this.userEditForm.value);

    // todo save into data.json
  }

  public logout() {
    this.usersService.clearUserData();
    this.router.navigate(['/']);
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

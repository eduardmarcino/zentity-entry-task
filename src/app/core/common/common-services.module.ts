import { NgModule } from '@angular/core';
import { SeoService } from '../services/seo.service';
import { UsersService } from '../services/api/users/users.service';
import { PasswordGuardService } from '../services/routing/guards/password.guard.service';
import { StyleService } from '../services/utils/style.service';
import { AuthorizedGuardService } from '../services/routing/guards/authorized.guard.service';

const providers = [SeoService, UsersService, PasswordGuardService, AuthorizedGuardService, StyleService];

@NgModule({
  providers: providers,
})
export class CommonServicesModule {}

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CommonComponentsModule } from '../common/common-components.module';
import { SplashComponent } from './splash.component';

export const routes: Routes = [
  {
    path: '',
    component: SplashComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    CommonComponentsModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    RouterModule.forChild(routes),
  ],
  declarations: [SplashComponent],
})
export class SplashModule {}

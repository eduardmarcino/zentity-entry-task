import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AbstractControl, Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../services/api/users/users.service';
import { take } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { UserDetail, User } from '../services/api/users/users.interface';
import { MustMatch } from '../services/utils/form-match.validator';
import { Router } from '@angular/router';

@Component({
  selector: 'password',
  styleUrls: ['./password.component.scss'],
  templateUrl: 'password.component.html',
})
export class PasswordComponent implements OnInit, OnDestroy {
  public users: User[];
  public userByUserName: User;
  public passwordForm: FormGroup;
  private ngUnsubscribe: Subject<boolean> = new Subject<boolean>();

  constructor(private formBuilder: FormBuilder, private usersService: UsersService, private router: Router) {}

  public ngOnInit(): void {
    this.passwordForm = this.formBuilder.group(
      {
        password: [null, [Validators.minLength(9)]],
        confirmPassword: [null, [Validators.minLength(9)]],
      },
      {
        validator: MustMatch('password', 'confirmPassword'),
      }
    );

    this.usersService
      .users()
      .pipe(take(1))
      .subscribe((data: User[]) => {
        this.users = data;
      });

    this.usersService.userByUserName.pipe(take(1)).subscribe((user: User) => {
      this.userByUserName = user;
    });
  }

  public get password(): AbstractControl {
    return this.passwordForm.get('password');
  }

  public get confirmPassword(): AbstractControl {
    return this.passwordForm.get('confirmPassword');
  }

  public onSubmit() {
    if (!this.passwordForm.valid) {
      return;
    }

    if (this.userByUserName.user.password === this.passwordForm.value.password) {
      this.usersService.loggedUser.next(this.userByUserName);
      this.router.navigate(['profile']);
    } else {
      this.password.setErrors({ wrongPassword: true });
    }
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

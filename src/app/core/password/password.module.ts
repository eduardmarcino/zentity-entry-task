import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CommonComponentsModule } from '../common/common-components.module';
import { PasswordComponent } from './password.component';

export const routes: Routes = [
  {
    path: '',
    component: PasswordComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    CommonComponentsModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
  ],
  declarations: [PasswordComponent],
})
export class PasswordModule {}

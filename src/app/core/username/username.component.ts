import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../services/api/users/users.service';
import { filter, take } from 'rxjs/operators';
import { User } from '../services/api/users/users.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'username',
  styleUrls: ['./username.component.scss'],
  templateUrl: 'username.component.html',
})
export class UsernameComponent implements OnInit {
  public userNameForm: FormGroup;
  private users: User[];

  constructor(private formBuilder: FormBuilder, private usersService: UsersService, private router: Router) {}

  public ngOnInit(): void {
    this.userNameForm = this.formBuilder.group({
      username: [null, [Validators.pattern('[A-zÀ-ž0-9 ]+'), Validators.minLength(3), Validators.maxLength(20)]],
    });

    this.usersService
      .users()
      .pipe(take(1))
      .subscribe((data: User[]) => {
        this.users = data;
      });

    // set username into form if was filled in previous step
    this.usersService.userByUserName
      .pipe(
        filter((user: User) => user !== null),
        take(1)
      )
      .subscribe((user: User) => {
        this.username.setValue(user.user.displayName);
      });
  }

  public get username(): AbstractControl {
    return this.userNameForm.get('username');
  }

  public onSubmit() {
    if (!this.userNameForm.valid) {
      return;
    }
    if (!this.userNameForm.value.username) {
      this.username.setErrors({ required: true });
      return;
    }

    const foundUser: User = this.usersService.findUserByUserName(this.userNameForm.value.username, this.users);

    if (!foundUser) {
      this.username.setErrors({ userNotFound: true });
    } else {
      this.usersService.userByUserName.next(foundUser);
      this.router.navigate(['/password']);
    }
  }
}

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CommonComponentsModule } from '../common/common-components.module';
import { UsernameComponent } from './username.component';

export const routes: Routes = [
  {
    path: '',
    component: UsernameComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    CommonComponentsModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
  ],
  declarations: [UsernameComponent],
})
export class UsernameModule {}

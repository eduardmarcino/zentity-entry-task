import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { LocationStrategy } from '@angular/common';

@Component({
  selector: 'header-control',
  templateUrl: 'header.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  @Input() public backButton: boolean = false;
  @Input() public userAvatar: boolean = false;

  constructor(private location: LocationStrategy) {}

  public goBack(): void {
    this.location.back();
  }
}

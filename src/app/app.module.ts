import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import * as Sentry from '@sentry/browser';
import { SentryErrorHandler } from './core/services/sentry-error-handler';
import { environment } from '../environments/environment';
import { CommonServicesModule } from './core/common/common-services.module';
import { registerLocaleData } from '@angular/common';
import localeCs from '@angular/common/locales/cs';
import { MissingTranslationHandler, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslationsLoader } from './core/services/translations/translate.loader';
import { MissingTranslationsHandler } from './core/services/translations/missing-translations.handler';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

registerLocaleData(localeCs, 'cs');

if (environment.production) {
  // Sentry.init({
  //   dsn: 'fill sentry DSN',
  //   attachStacktrace: true,
  //   whitelistUrls: [/whitelist-url\.cz/]
  // });
}

export function TranslationsLoaderFactory() {
  return new TranslationsLoader();
}

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    CommonServicesModule,
    TranslateModule.forRoot({
      loader: { provide: TranslateLoader, useFactory: TranslationsLoaderFactory },
      missingTranslationHandler: { provide: MissingTranslationHandler, useClass: MissingTranslationsHandler },
    }),
    BrowserAnimationsModule,
  ],
  declarations: [AppComponent],
  providers: [
    { provide: ErrorHandler, useClass: SentryErrorHandler },
    { provide: 'windowObject', useFactory: windowFactory },
  ],
  exports: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

export function windowFactory() {
  return window;
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PasswordGuardService } from './core/services/routing/guards/password.guard.service';
import { BackgroundBodyClassEnum } from './core/services/utils/style.service';
import { AuthorizedGuardService } from './core/services/routing/guards/authorized.guard.service';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./core/splash/splash.module').then((m) => m.SplashModule),
    data: {
      title: 'Introduction',
    },
  },
  {
    path: 'username',
    loadChildren: () => import('./core/username/username.module').then((m) => m.UsernameModule),
    data: {
      title: 'Set Username',
    },
  },
  {
    path: 'password',
    canActivate: [PasswordGuardService],
    loadChildren: () => import('./core/password/password.module').then((m) => m.PasswordModule),
    data: {
      title: 'Set Password',
    },
  },
  {
    path: 'profile',
    canActivate: [AuthorizedGuardService],
    loadChildren: () => import('./core/profile/profile.module').then((m) => m.ProfileModule),
    data: {
      title: 'Edit your profile',
      background: BackgroundBodyClassEnum.WHITE,
    },
  },
  {
    path: '404',
    loadChildren: () => import('./core/not-found/not-found.module').then((m) => m.NotFoundModule),
    data: {
      title: '404 - not found',
      description: 'Not found page',
      keywords: '',
    },
  },
  { path: '**', redirectTo: '404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled', relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}

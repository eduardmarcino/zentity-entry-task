import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { SeoService } from './core/services/seo.service';
import { TranslateService } from '@ngx-translate/core';
import { BackgroundBodyClassEnum, StyleService } from './core/services/utils/style.service';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private seoService: SeoService,
    private translationsService: TranslateService,
    private styleService: StyleService
  ) {}

  public ngOnInit(): void {
    this.router.events
      .pipe(
        filter((e: RouterEvent) => e instanceof NavigationEnd),
        map(() => this.route),
        map((route: ActivatedRoute) => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        })
      )
      .subscribe((data: ActivatedRoute) => {
        const background: BackgroundBodyClassEnum =
          (data.snapshot.data.background as BackgroundBodyClassEnum) || BackgroundBodyClassEnum.DARK;
        this.styleService.setBodyBackground(background);
        this.seoService.configureSeoOnNavigation(data);
      });

    // init translation service
    this.translationsService.use('cs');
  }
}

## Zentity Entry task

Run app

1. npm install
2. npm run start

---

Build app

1. npm install
2. npm run build

---

How to run of build app

1. install live-server "npm install live-server -g"
2. build app
3. in folder /dist run command live-server
4. app will open in browser
